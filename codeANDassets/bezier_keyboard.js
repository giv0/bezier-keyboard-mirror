const canvas = document.getElementById("fullpage");
canvas.style = 'top:0px;left:0px;position:fixed;z-index: 0';
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
const ctx = canvas.getContext("2d");

document.getElementById('secrets').style = 'top:10px;left:10px;position:fixed;z-index: 9999';
document.getElementById('usernamelabel').style = 'top:40px;left:10px;position:fixed;z-index: 9999';
document.getElementById('username').style = 'top:40px;left:100px;;position:fixed;z-index: 9999';
document.getElementById('passwordlabel').style = 'top:70px;left:10px;position:fixed;z-index: 9999';
document.getElementById('password').style = 'top:70px;left:100px;position:fixed;z-index: 9999';

var bezierProgress = 0;
var lastUpdate = Date.now();

var bezierPoints = [];
var pascal = [];
var bezierCurve = [];

function Point(X, Y) {
    this.x = X | 0;
    this.y = Y | 0;
}

function CalculateBezier() {
    bezierCurve = [];

    var totalCurve = (bezierPoints.length - 1) * 100;
    var points = bezierPoints;
    var pascalRow = pascal[pascal.length - 1];

    for (var i = 0; i <= totalCurve; i++) {
        var progress = i / totalCurve;
        var pointX = 0;
        var pointY = 0;

        for (var j = 0; j < points.length; j++) {
            pointX += pascalRow[j] * Math.pow(progress, j) * Math.pow(1 - progress, points.length - j - 1) * points[j].x;
            pointY += pascalRow[j] * Math.pow(progress, j) * Math.pow(1 - progress, points.length - j - 1) * points[j].y;
        }

        bezierCurve.push(new Point(pointX, pointY));
    }
}

function SubmitForm() {
    
}

var currentBox = 0;
var boxes = [];
var boxVal = ["", ""];

boxes.push(document.getElementById('username'));
boxes.push(document.getElementById('password'));

function NextBox() {
    dragging = false;
    overIndex = -1;
    bezierPoints = [];
    pascal = [];
    bezierCurve = [];
}

function Key(which, where) {
    this.key = which;
    this.point = where;
}

var currentPoint = new Point(0, 0);
var overIndex = -1;
var dragging = false;
var mousePos = { x: 0, y: 0 };
var showcurve = true;

var inKey = false;
var isCaps = false;
var isShift = false;
var allKeys = [];

allKeys.push(new Key("shift", new Point(Math.random()*canvas.width, Math.random()*canvas.height)));
allKeys.push(new Key("caps lock", new Point(Math.random()*canvas.width, Math.random()*canvas.height)));
allKeys.push(new Key("space", new Point(Math.random()*canvas.width, Math.random()*canvas.height)));

var letters = 'abcdefghijklmnopqrstuvwxyz'.split('');
var capsLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
for (var i = 0; i < 26; i++) {
    allKeys.push(new Key(i, new Point(Math.random()*canvas.width, Math.random()*canvas.height)));
}

function loop() {
    var now = Date.now();
    var dt = (now - lastUpdate) / 1000;
    lastUpdate = now;

    //bezierProgress = bezierProgress >= 1 ? 0 : bezierProgress + dt / 5;
    ctx.fillStyle = "#ffffff";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    if (!dragging) {
        overIndex = -1;
    }

    ctx.strokeStyle = "#000000";
    ctx.fillStyle = "#000000";
    ctx.font = '24px serif';
    ctx.lineWidth = 1;
    for (var i = 0; i < allKeys.length; i++) {
        ctx.beginPath();
        ctx.arc(allKeys[i].point.x, allKeys[i].point.y, 20, 0, Math.PI * 2);
        ctx.stroke();
        var keyText = allKeys[i].key;
        if (typeof keyText === 'number') {
            keyText = letters[allKeys[i].key];
        }
        ctx.fillText(keyText, allKeys[i].point.x-ctx.measureText(allKeys[i].key).width/2, allKeys[i].point.y+6);
    }

    if (bezierPoints.length > 0) {
        ctx.beginPath();
        ctx.moveTo(bezierPoints[0].x, bezierPoints[0].y);
        for (var i = 1; i < bezierPoints.length; i++) {
            ctx.lineTo(bezierPoints[i].x, bezierPoints[i].y);
        }
        ctx.strokeStyle = "#000000";
        ctx.lineWidth = 3;
        ctx.stroke();
    }

    var boxBuffer = "";
    if (showcurve && bezierCurve.length > 0) {
        ctx.beginPath();
        ctx.moveTo(bezierCurve[0].x, bezierCurve[0].y);

        inKey = false;
        isCaps = false;
        isShift = false;

        for (var i = 1; i < bezierCurve.length; i++) {
            ctx.lineTo(bezierCurve[i].x, bezierCurve[i].y);

            if (!inKey) {
                for (var j = 0; j < allKeys.length; j++) {
                    if (Math.abs(Math.hypot(bezierCurve[i].x-allKeys[j].point.x, bezierCurve[i].y-allKeys[j].point.y)) <= 20) {
                        switch (allKeys[j].key) {
                            case "shift":
                                isShift = !isShift;
                                break;
    
                            case "caps lock":
                                isCaps = !isCaps;
                                break;
    
                            case "space":
                                boxBuffer += " ";
                                break;
    
                            case "enter":
                                boxVal[currentBox] = boxBuffer;
                                NextBox();
                                return;
    
                            default:
                                if (isCaps) {
                                    if (isShift) {
                                        boxBuffer += letters[allKeys[j].key];
                                        isShift = false;
                                    } else {
                                        boxBuffer += capsLetters[allKeys[j].key];
                                    }
                                } else {
                                    if (isShift) {
                                        boxBuffer += capsLetters[allKeys[j].key];
                                        isShift = false;
                                    } else {
                                        boxBuffer += letters[allKeys[j].key];
                                    }
                                }
                        }
                        inKey = true;
                    }
                }
            } else {
                var notInAny = true;
                var hasPrinted = false
                for (var j = 0; j < allKeys.length; j++) {
                    if (Math.abs(Math.hypot(bezierCurve[i].x-allKeys[j].point.x, bezierCurve[i].y-allKeys[j].point.y)) <= 20) {
                        notInAny = false;
                        
                        /* if (!hasPrinted) {
                            ctx.strokeStyle = "#00ff00";
                            ctx.lineWidth = 1;
                            ctx.beginPath();
                            ctx.moveTo(bezierCurve[i].x, bezierCurve[i].y);
                            ctx.lineTo(allKeys[j].point.x, allKeys[j].point.y);
                            ctx.stroke();
    
                            //console.log(`${bezierCurve[i].x-allKeys[j].point.x}, ${bezierCurve[i].y-allKeys[j].point.y}`);
                            //hasPrinted = true;
                        }
                        */
                    }
                }

                if (notInAny) {
                    inKey = false;
                }
            }
        }
        ctx.strokeStyle = "#0000ff";
        ctx.lineWidth = 5;
        ctx.stroke();
    }
    console.log(currentBox);
    boxVal[currentBox] = boxBuffer;
    
    boxes[0].value = boxVal[0];
    boxes[1].value = boxVal[1];

    ctx.fillStyle = "#ff0000";
    for (var i = 0; i < bezierPoints.length; i++) {
        ctx.beginPath();
        ctx.arc(bezierPoints[i].x, bezierPoints[i].y, 10, 0, Math.PI * 2);
        ctx.fill();

        if (!dragging && Math.hypot(mousePos.x - bezierPoints[i].x, mousePos.y - bezierPoints[i].y) <= 10) {
            document.getElementById("fullpage").style.cursor = "pointer";
            overIndex = i;
        }
    }

    if (!dragging && overIndex == -1) {
        document.getElementById("fullpage").style.cursor = "default";
    }

    window.requestAnimationFrame(loop);
}

window.requestAnimationFrame(loop);

bezierPoints.push(new Point(canvas.width/2, canvas.height/2));
pascal.push([1]);
CalculateBezier();

canvas.addEventListener('mousedown', (e) => {
    if (e.button == 0) {
        if (overIndex > 0) {
            dragging = true;
        }
    } else if (e.button == 2) {
        bezierPoints.push(new Point(e.offsetX, e.offsetY));

        if (pascal.length == 0) {
            pascal.push([1]);

            CalculateBezier();
        } else {
            var triangleRow = [];
            var lastRow = pascal[pascal.length - 1];
            for (var i = 0; i < lastRow.length + 1; i++) {

                if (i > 0 && i < lastRow.length) {
                    triangleRow.push(lastRow[i - 1] + lastRow[i]);
                } else {
                    triangleRow.push(1);
                }

            }

            pascal.push(triangleRow);
            console.log(triangleRow);

            CalculateBezier();
        }
    }
});

canvas.addEventListener('mouseup', (e) => {
    if (e.button == 0) {
        dragging = false;
    }
});

canvas.addEventListener('mousemove', (e) => {
    mousePos.x = e.offsetX;
    mousePos.y = e.offsetY;

    if (dragging) {
        bezierPoints[overIndex].x = e.offsetX;
        bezierPoints[overIndex].y = e.offsetY;

        CalculateBezier();
    }
});

document.addEventListener("contextmenu", function (e) {
    e.preventDefault();
}, false);

function disabledEvent(e) {
    if (e.stopPropagation) {
        e.stopPropagation();
    } else if (window.event) {
        window.event.cancelBubble = true;
    }
    e.preventDefault();
    return false;
}

document.addEventListener('keydown', (e) => {
    switch (e.key) {

        case "Enter":
            bezierPoints = [];
            pascal = [];
            bezierCurve = [];
            currentPoint = new Point(0, 0);
            bezierPoints.push(new Point(canvas.width/2, canvas.height/2));
            pascal.push([1]);
            CalculateBezier();
            currentBox++;
            break;
    }

    if (currentBox > 1) {
        alert(`Successfully logged in!\nUsername: ${boxVal[0]}\nPassword: ${boxVal[1]}`);
        var amogus = document.getElementById('amogus');
        amogus.style = 'top:0px;left:0px;position:fixed;z-index: 9999999';
        amogus.width = window.innerWidth;
        amogus.height = window.innerHeight; 
    }
});

canvas.addEventListener('touchstart', (e) => {
    bezierPoints.push(new Point(e.touches[0].pageX, e.touches[0].pageY));

        if (pascal.length == 0) {
            pascal.push([1]);

            CalculateBezier();
        } else {
            var triangleRow = [];
            var lastRow = pascal[pascal.length - 1];
            for (var i = 0; i < lastRow.length + 1; i++) {

                if (i > 0 && i < lastRow.length) {
                    triangleRow.push(lastRow[i - 1] + lastRow[i]);
                } else {
                    triangleRow.push(1);
                }

            }

            pascal.push(triangleRow);
            console.log(triangleRow);

            CalculateBezier();
        }
});